class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(players)
    @players = players
    # @board = board
  end

  def guesser
    @players[:guesser]
  end

  def referee
    @players[:referee]
  end

  def setup
    length = @players[:referee].pick_secret_word
    @players[:guesser].register_secret_length(length)
    @board = Array.new(length)
    # p @player[:guesser]
  end

  def take_turn
    guess = @players[:guesser].guess
    @players[:referee].check_guess(guess)
    self.update_board
    @players[:guesser].handle_response
  end

  def update_board
  end
end

class HumanPlayer

end

class ComputerPlayer
  def initialize(dictionary)
    @dictionary = dictionary
  end

  def pick_secret_word
    @dictionary.join('').length
  end

  def check_guess(given_letter)
    found_letters = []

    @dictionary.join.chars.each_with_index do |letter, idx|
      found_letters << idx if given_letter == letter
    end

    # raise "incorrect guess!" if found_letters == []
    found_letters
  end

  def register_secret_length(length)
    @candidate_words = @dictionary.select { |word| word.length == length }
  end

  def guess(board)
    letter = "k"
  end

  def handle_response(letter, arr)
  end

  def candidate_words
    @candidate_words
  end

end
